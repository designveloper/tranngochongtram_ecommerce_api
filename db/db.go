package db

import (
	"sync"

	"github.com/jmoiron/sqlx"
	// Postgres driver for the database/sql package
	"github.com/dsv/aware/utils"
	_ "github.com/lib/pq"
)

var (
	db   *sqlx.DB
	once sync.Once
)

// GetInstance return a connection to current database instance
func GetInstance() *sqlx.DB {
	once.Do(getConnection)
	return db
}

// Singleton
// http://marcio.io/2015/07/singleton-pattern-in-go/
func getConnection() {
	dbInstance, err := sqlx.Connect("postgres", utils.GetConfig().DbURL)
	if err != nil {
		panic(err)
	}

	db = dbInstance
}
