-- migrate:up
CREATE TABLE users (
  ID SERIAL PRIMARY KEY,
  NAME VARCHAR(300) NOT NULL,
  EMAIL VARCHAR(400) NOT NULL,
  PASSWORD VARCHAR(400) NOT NULL
);

INSERT INTO users(name, email, password) VALUES 
('abc', '1@yopmail.com','1234'),
('def', '2@yopmail.com','1234')

-- migrate:down
DROP TABLE users
