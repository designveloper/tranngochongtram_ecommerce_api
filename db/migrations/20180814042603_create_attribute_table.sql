-- migrate:up
CREATE TABLE attribute (
  ID SERIAL PRIMARY KEY,
  NAME VARCHAR(200) NOT NULL,
  DEACTIVE BOOLEAN DEFAULT FALSE NOT NULL
);

INSERT INTO attribute(name) VALUES 
('Size'), ('Color'), ('Brand'), ('Price');

-- migrate:down
DROP TABLE attribute
