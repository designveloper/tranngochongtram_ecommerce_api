-- migrate:up
CREATE TABLE category (
  ID SERIAL PRIMARY KEY,
  NAME VARCHAR(500) NOT NULL,
  DEACTIVE BOOLEAN DEFAULT FALSE NOT NULL,
  IMG_URL VARCHAR(3000) NOT NULL
);

INSERT INTO category(name, img_url) VALUES 
('Men', 'https://lh3.googleusercontent.com/t7YjrY9Se9hfTI_TgWSmH_4B0VbIAM_ZcwxveVrKSPEU6fXEcVRUa-aN0mnnpr7AIG20NztwMyDOORMY3aUEYlb9TV4IQpzsyYe6r5QoNq-ucP9E10sfgnuYSbETkyoUZh32W91sek2vG0ZiKRAv5t0b-3kZJ_mjQOYxJQmqRKJb-K6B_kq4laQp9US3EYs9zgZetmloFmv253QuPd4-6yc_JORwCdHb2JhT6_J12x0zAmHs0qbr1oqEJzEOm_sis0uV4s2rcFyQMAX-PIoSpcd5X7wcUamZijayBlY2efWBVQCsTMxzch3n2bB_LH3nZlBbeYVhs2t32udc_R7cs8pDWh9PRqNEBz5KCWJRqfIHfOPUUaPqDADpcYRkgS9sKwPKZ0wq1TpNNUpDUAjTcDJMcMxTi9e7j1y05gVf1UPRNgXUHu9OUXUQZj7IiXPISRbV6mbFhxBu3WIZSpy4FbWStqM-XjROp19sx87z8CuM07fXRWjXAJ_7m76rTQdIIwjS5AhJIOgw1dYC5IHQJphUCATb2ttj8HVINRIEfWFMkQTyqiHqP7ANjxjYNIn0=w3356-h1818'), 
('Ladies', 'https://lh3.googleusercontent.com/-zAi5KgKEnPvZ7mRzuQI3QRzZ5wn-VXZrT6IQNn7PPJVif9nZh61kZ7FP4IDtFl3fAYy_tMfbkoJAgNyC9ck5yVi7KL_YA36-6TAIZEoL5e90GSRSIvBZkVmJFzXXQpvAQNjGEQQBdZddoD50fNIsvYv7GlvkXfs66RdXJxjr3hOVPEeCi2i67pXFjf35vE0lOGEpCzySCrAyvIT1wWXnZ9MvqT91nm98TlcvyxjG8ezc1airqJ4TZbjZ-B5FWeeg9U062KlS6qfAbU905MHAfIee6BKYuAeDnuSmUOasyfww_hL3Pygo3ImKXo1KS3CeBPTnBGdbfFEHz8ZBrBcjO1zQorWpIxIu_q4Fz-MPWb01K2O1LKRlIGeEKAwfBZG5KyytZg3Pp91EsTJQEn8A8PndF1hHSuJHnsgW339pDP04L3DbzOpT1Ehc9HY8okEAaosenQ874u4RDMAoRWAIq2DqXDL8ltevzqHvVj4uPi2NhXAr95oRVFHhyU0AKpEiuiMCkJbl-9BhI7bTCFzR3Y_dn_OJqo9satVxI2w-uyCaZEBNlyveqD9aEFNjVvG=w3354-h1906'),
('Girls', 'https://lh3.google.com/u/2/d/11BQz6N39zHR1iBvhwNtyVL2-oWSRgjwL=w2856-h1906-iv1'), 
('Boys', 'https://lh3.google.com/u/2/d/18QdWttnmXk4uzh8W-7Z8LfMssrXqfT4l=w2856-h1906-iv1');


-- migrate:down
DROP TABLE category

