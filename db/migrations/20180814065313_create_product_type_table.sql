-- migrate:up
CREATE TABLE product_type(
  ID SERIAL PRIMARY KEY,
  NAME VARCHAR(800) NOT NULL
);

INSERT INTO product_type(name) VALUES 
('Rompers/Jumpsuits'), ('Casual dresses'), ('Going out dresses'), ('Party/Ocassion dresses'), ('Mini dresses'), ('Maxi/Midi dresses'), ('Sets');

-- migrate:down
DROP TABLE product_type

