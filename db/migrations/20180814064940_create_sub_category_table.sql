-- migrate:up
CREATE TABLE sub_category (
  ID SERIAL PRIMARY KEY,
  NAME VARCHAR(500) NOT NULL,
  CATEGORY_ID INT REFERENCES category(id)
);

INSERT INTO sub_category(name, category_id) VALUES 
('Tops', 2),
('Bottoms', 2),
('Dresses', 2),
('Jackets', 2),
('Shoes', 2),
('Accessories', 2),
('Sale', 2);

-- migrate:down
DROP TABLE sub_category

