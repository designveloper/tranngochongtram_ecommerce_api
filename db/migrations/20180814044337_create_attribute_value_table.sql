-- migrate:up
CREATE TABLE attribute_value (
  ID SERIAL PRIMARY KEY,
  ATTRIBUTE_ID INT REFERENCES attribute(id),
  DEACTIVE BOOLEAN DEFAULT false NOT NULL,
  ATTRIBUTE_VALUE VARCHAR(300) NOT NULL
);

INSERT INTO attribute_value(attribute_value, attribute_id) VALUES 
('S', 1),
('M', 1),
('L', 1),
('White', 2),
('Black', 2),
('Yellow', 2),
('Grey', 2),
('Brown', 2),
('Green', 2),
('Red', 2),
('Zara',3),
('Mango',3);


-- migrate:down
DROP TABLE attribute_value
