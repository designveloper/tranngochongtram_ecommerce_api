-- migrate:up
CREATE TABLE product (
  ID SERIAL PRIMARY KEY,
  NAME VARCHAR(1000) NOT NULL,
  DEACTIVE BOOLEAN DEFAULT false NOT NULL,
  PRICE BIGINT NOT NULL,
  PRODUCT_TYPE_ID INT REFERENCES product_type(id),
  SUB_CATEGORY_ID INT REFERENCES sub_category(id),
  IMG_URL VARCHAR(4000) NOT NULL
);

INSERT INTO product(name, price, product_type_id, sub_category_id,img_url) VALUES 
('Collete Stretch Linen Minidress', 70, 5, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Geo Print Cutout Dress', 69, 3, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Button-Down Denim Mini Dress', 50, 2, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Hermosa Ladder Lace Midi Dress', 79, 6, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Surplice Gingham Jumpsuit', 55, 3, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Arabella Textured Maxi Skirt', 63, 6, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Lace-Trim Surplice Mini Dress', 60, 4, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Faux Suede Tube Dress', 72, 5, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Stretch Linen Romper', 79,1, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('T-Shirt Midi Dress', 35, 1, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Ruched Asymmetrical Dress', 63, 6, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Houndstooth Lace-Up Jumpsuit', 60, 4, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Floral Surplice Dress', 43, 5, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Polka Dot Skater Dress', 37,1, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Medallion Print Maxi Dress', 70, 3, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Ditsy Floral Print Fit & Flare Dress', 63, 6, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Marled Tank Dress', 61, 4, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Off-The-Shoulder Denim Dress', 56, 5, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Tropical Floral Print Mini Dress', 77,1, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Ribbed Knit Midi Cami Dress', 44, 1, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Dolphin-Hem Shirt Dress', 67, 6, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Eyelash Lace Maxi Dress', 53, 4, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Varsity-Striped T-Shirt Dress', 65, 5, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2'),
('Checkered Race Track Romper', 80,1, 3, 'https://lh3.google.com/u/2/d/1UZ60VNjmZ0huJIxEpLpp1-bzVUNGoDFo=w3360-h1818-iv2');

-- migrate:down
DROP TABLE product

