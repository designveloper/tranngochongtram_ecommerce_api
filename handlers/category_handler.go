package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/dsv/aware/services"
)

// CategoryHandler interface
type CategoryHandler interface {
	GetAllCategories(w http.ResponseWriter, r *http.Request)
}

var cHandler *categoryHandler

type categoryHandler struct{}

// GetCategoryHandler return instance of CategoryHandler
func GetCategoryHandler() CategoryHandler {
	once.Do(func() {
		cHandler = &categoryHandler{}
	})

	return cHandler
}

func (h *categoryHandler) GetAllCategories(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	categories := services.GetCategoryService().GetAllCategories()
	json.NewEncoder(w).Encode(categories)
}
