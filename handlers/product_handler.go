package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/dsv/aware/services"
)

// ProductHandler interface
type ProductHandler interface {
	FilterProducts(w http.ResponseWriter, r *http.Request)
}

var pHandler *productHandler

type productHandler struct{}

// GetProductHandler return instance of ProductHandler
func GetProductHandler() ProductHandler {
	once.Do(func() {
		pHandler = &productHandler{}
	})

	return pHandler
}

func (h *productHandler) FilterProducts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	subCateID, isExist := r.URL.Query()["sub-category-id"]
	if !isExist {
		w.Write([]byte("Must specify sub-category-id"))
		return
	}

	subID, err := strconv.Atoi(subCateID[0])
	if err != nil {
		panic(err)
	}

	proID := 0
	proTypeID, isExist := r.URL.Query()["product-type-id"]
	if !isExist {
		proID = 0
	} else {
		proID, err = strconv.Atoi(proTypeID[0])
		if err != nil {
			panic(err)
		}
	}

	var proAttrValIDs []int
	proAttrVal, isExist := r.URL.Query()["attribute-value-ids"]
	if !isExist {
		proAttrValIDs = nil
	} else {
		for _, item := range proAttrVal[0] {
			if string(item) == "," {
				continue
			}
			id, err := strconv.Atoi(string(item))
			if err != nil {
				panic(err)
			}
			proAttrValIDs = append(proAttrValIDs, id)
		}
	}

	products := services.GetProductService().FilterProducts(proAttrValIDs, subID, proID)
	json.NewEncoder(w).Encode(products)
}
