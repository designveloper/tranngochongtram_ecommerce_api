package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/dsv/aware/services"
)

// AttributeHandler interface
type AttributeHandler interface {
	GetAllAttributes(w http.ResponseWriter, r *http.Request)
}

var aHandler *attributeHandler

type attributeHandler struct{}

// GetAttributeHandler return instance of AttributeHandler
func GetAttributeHandler() AttributeHandler {
	once.Do(func() {
		aHandler = &attributeHandler{}
	})

	return aHandler
}

func (h *attributeHandler) GetAllAttributes(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	attributes := services.GetAttributeService().GetAllAttributes()
	json.NewEncoder(w).Encode(attributes)
}
