package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/dsv/aware/services"
)

// ProductTypeHandler interface
type ProductTypeHandler interface {
	GetAllProductTypes(w http.ResponseWriter, r *http.Request)
}

var ptHandler *productTypeHandler

type productTypeHandler struct{}

// GetProductTypeHandler return instance of ProductTypeHandler
func GetProductTypeHandler() ProductTypeHandler {
	once.Do(func() {
		ptHandler = &productTypeHandler{}
	})

	return ptHandler
}

func (h *productTypeHandler) GetAllProductTypes(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	productTypes := services.GetProductTypeService().GetAllProductTypes()
	json.NewEncoder(w).Encode(productTypes)
}
