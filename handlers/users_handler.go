package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	models "github.com/dsv/aware/models/request"
	"github.com/dsv/aware/services"
)

// UsersHandler interface
type UsersHandler interface {
	GetUserByID(w http.ResponseWriter, r *http.Request)
	CreateUser(w http.ResponseWriter, r *http.Request)
}

var uHandler *userHandler

type userHandler struct{}

// GetUsersHandler return instance of UsersHandler
func GetUsersHandler() UsersHandler {
	once.Do(func() {
		uHandler = &userHandler{}
	})

	return uHandler
}

// GetUserById handler get user by id
func (h *userHandler) GetUserByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		panic(err)
	}
	item := services.GetUserService().GetUserByID(id)
	json.NewEncoder(w).Encode(item)
}

func (h *userHandler) CreateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	user := models.UserRequest{}
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		panic(err)
	}
	user.ID = services.GetUserService().CreateUser(&user)
	json.NewEncoder(w).Encode(user)
}
