package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	models "github.com/dsv/aware/models/response"

	"github.com/dsv/aware/services"
)

// SubCategoryHandler interface
type SubCategoryHandler interface {
	GetSubCategories(w http.ResponseWriter, r *http.Request)
}

var scHandler *subCategoryHandler

type subCategoryHandler struct{}

// GetSubCategoryHandler return instance of SubCategoryHandler
func GetSubCategoryHandler() SubCategoryHandler {
	once.Do(func() {
		scHandler = &subCategoryHandler{}
	})

	return scHandler
}

func (h *subCategoryHandler) GetSubCategories(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	condition, conditionExist := r.URL.Query()["category-id"]
	sv := services.GetSubCategoryService()
	var subCategories []*models.SubCategoryResponse

	if conditionExist {
		id, _ := strconv.Atoi(condition[0])
		subCategories = sv.GetSubCategoriesByID(id)
	} else {
		subCategories = sv.GetAllSubCategories()
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(subCategories)
}
