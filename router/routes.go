package router

import (
	"net/http"

	"github.com/dsv/aware/handlers"
)

// Route type description
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// Routes contains all routes
type Routes []*Route

var routes = Routes{
	&Route{
		Name:        "GetUserById",
		Method:      "GET",
		Pattern:     "/user/{id}",
		HandlerFunc: handlers.GetUsersHandler().GetUserByID,
	},
	&Route{
		Name:        "CreateUser",
		Method:      "POST",
		Pattern:     "/user",
		HandlerFunc: handlers.GetUsersHandler().CreateUser,
	},
	&Route{
		Name:        "GetSubCategories",
		Method:      "GET",
		Pattern:     "/sub-categories",
		HandlerFunc: handlers.GetSubCategoryHandler().GetSubCategories,
	},
	&Route{
		Name:        "GetAllAttributes",
		Method:      "GET",
		Pattern:     "/attributes",
		HandlerFunc: handlers.GetAttributeHandler().GetAllAttributes,
	},
	&Route{
		Name:        "GetAllCategories",
		Method:      "GET",
		Pattern:     "/categories",
		HandlerFunc: handlers.GetCategoryHandler().GetAllCategories,
	},
	&Route{
		Name:        "GetAllProductTypes",
		Method:      "GET",
		Pattern:     "/product-types",
		HandlerFunc: handlers.GetProductTypeHandler().GetAllProductTypes,
	},
	&Route{
		Name:        "FilterProducts",
		Method:      "GET",
		Pattern:     "/products",
		HandlerFunc: handlers.GetProductHandler().FilterProducts,
	},
}
