package repositories

import (
	models "github.com/dsv/aware/models/data"
)

type ProductTypeRepository interface {
	GetAllProductTypes() ([]*models.ProductType, error)
}

var (
	ptRepo *productTypeRepo
)

type productTypeRepo struct {
}

// GetProductTypeRepository return a instance of ProductTypeRepository
func GetProductTypeRepository() ProductTypeRepository {
	once.Do(func() {
		ptRepo = &productTypeRepo{}
	})

	return ptRepo
}

func (r *productTypeRepo) GetAllProductTypes() ([]*models.ProductType, error) {
	query := `select * from product_type`

	productTypes := []*models.ProductType{}

	err := database.Select(&productTypes, query)
	if err != nil {
		return nil, err
	}
	return productTypes, nil
}
