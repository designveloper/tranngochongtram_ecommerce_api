package repositories

import (
	models "github.com/dsv/aware/models/data"
)

type AttributeRepository interface {
	GetAllAttributes() ([]*models.Attribute, error)
}

var (
	aRepo *attrRepo
)

type attrRepo struct {
}

// GetAttributeRepository return a instance of AttributeRepository
func GetAttributeRepository() AttributeRepository {
	once.Do(func() {
		aRepo = &attrRepo{}
	})

	return aRepo
}

func (r *attrRepo) GetAllAttributes() ([]*models.Attribute, error) {
	query := `select * from attribute`

	attributes := []*models.Attribute{}

	err := database.Select(&attributes, query)
	if err != nil {
		return nil, err
	}
	return attributes, nil
}
