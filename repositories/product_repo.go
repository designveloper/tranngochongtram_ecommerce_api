package repositories

import (
	"fmt"

	models "github.com/dsv/aware/models/data"
	"github.com/jmoiron/sqlx"
)

type ProductRepository interface {
	FilterProducts([]int, int, int) ([]*models.Product, error)
}

var (
	pRepo *productRepo
)

type productRepo struct {
}

// GetProductRepository return a instance of ProductRepository
func GetProductRepository() ProductRepository {
	once.Do(func() {
		pRepo = &productRepo{}
	})

	return pRepo
}

func (r *productRepo) FilterProducts(proAttrValueIds []int, subCateId int, proTypeId int) ([]*models.Product, error) {
	arg := map[string]interface{}{
		"subCateId":       subCateId,
		"proAttrValueIds": proAttrValueIds,
		"proTypeId":       proTypeId,
	}

	query := `select p.* from product p`

	if proAttrValueIds != nil {
		query += `
			right join product_attribute_value pav on pav.product_id = p.id and pav.attribute_value_id in (:proAttrValueIds)
		`
	}
	query += `
		where p.sub_category_id=:subCateId
	`
	if proTypeId > 0 {
		query += `
			and p.product_type_id=:proTypeId
		`
	}
	if proAttrValueIds != nil {
		query += `
			group by p.id
		`
	}

	query, args, err := sqlx.Named(query, arg)
	query, args, err = sqlx.In(query, args...)
	query = database.Rebind(query)
	products := []*models.Product{}

	err = database.Select(&products, query, args...)

	if err != nil {
		return nil, err
	}

	fmt.Println(query)

	return products, nil
}
