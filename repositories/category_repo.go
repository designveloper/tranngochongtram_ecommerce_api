package repositories

import (
	models "github.com/dsv/aware/models/data"
)

type CategoryRepository interface {
	GetAllCategories() ([]*models.Category, error)
}

var (
	cRepo *categoryRepo
)

type categoryRepo struct {
}

// GetCategoryRepository return a instance of CategoryRepository
func GetCategoryRepository() CategoryRepository {
	once.Do(func() {
		cRepo = &categoryRepo{}
	})

	return cRepo
}

func (r *categoryRepo) GetAllCategories() ([]*models.Category, error) {
	query := `select * from category`

	categories := []*models.Category{}

	err := database.Select(&categories, query)
	if err != nil {
		return nil, err
	}
	return categories, nil
}
