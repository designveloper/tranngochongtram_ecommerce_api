package repositories

import (
	models "github.com/dsv/aware/models/data"
)

type SubCategoryRepository interface {
	GetSubCategoriesByID(id int) ([]*models.SubCategory, error)
	GetAllSubCategories() ([]*models.SubCategory, error)
}

var (
	scRepo *subCategoryRepo
)

type subCategoryRepo struct {
}

// GetSubCategoryRepository return a instance of SubCategoryRepository
func GetSubCategoryRepository() SubCategoryRepository {
	once.Do(func() {
		scRepo = &subCategoryRepo{}
	})

	return scRepo
}

func (r *subCategoryRepo) GetSubCategoriesByID(id int) ([]*models.SubCategory, error) {
	query := `select * from sub_category where category_id=$1`

	subCategories := []*models.SubCategory{}

	err := database.Select(&subCategories, query, id)
	if err != nil {
		return nil, err
	}
	return subCategories, nil
}

func (r *subCategoryRepo) GetAllSubCategories() ([]*models.SubCategory, error) {
	query := `select * from sub_category`

	subCategories := []*models.SubCategory{}

	err := database.Select(&subCategories, query)
	if err != nil {
		return nil, err
	}
	return subCategories, nil
}
