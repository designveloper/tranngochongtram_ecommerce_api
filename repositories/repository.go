package repositories

import (
	"sync"

	"github.com/dsv/aware/db"

	"github.com/jmoiron/sqlx"
)

var (
	once     sync.Once
	database *sqlx.DB = db.GetInstance()
)
