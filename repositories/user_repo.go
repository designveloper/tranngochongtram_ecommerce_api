package repositories

import (

	// Postgres driver for the database/sql package

	models "github.com/dsv/aware/models/data"
	_ "github.com/lib/pq"
)

// UserRepository interface
type UserRepository interface {
	GetByID(id int) (*models.User, error)
	Create(newUser *models.User) (int, error)
}

var (
	uRepo *userRepo
)

type userRepo struct {
}

// GetUserRepository return a instance of UserRepository
func GetUserRepository() UserRepository {
	once.Do(func() {
		uRepo = &userRepo{}
	})

	return uRepo
}

func (r *userRepo) GetByID(id int) (*models.User, error) {
	query := `SELECT * FROM users WHERE id=$1`
	var user models.User

	err := database.Get(&user, query, id)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (r *userRepo) Create(newUser *models.User) (int, error) {
	query := `INSERT INTO users(name, email, password) values(:name, :email, :password) RETURNING id`
	var id int
	rows, err := database.NamedQuery(query, newUser)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		err := rows.Scan(&id)
		if err != nil {
			return 0, err
		}
	}
	return id, nil
}
