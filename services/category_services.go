package services

import (
	models "github.com/dsv/aware/models/response"
	"github.com/dsv/aware/repositories"
)

type CategoryServices interface {
	GetAllCategories() []*models.CategoryResponse
}

var csv *categoryService

type categoryService struct{}

// GetCategoryService return a instance of CategoryServices
func GetCategoryService() CategoryServices {
	once.Do(func() {
		csv = &categoryService{}
	})

	return csv
}

func (csv *categoryService) GetAllCategories() []*models.CategoryResponse {
	categories, err := repositories.GetCategoryRepository().GetAllCategories()
	if err != nil {
		panic(err)
	}

	res := []*models.CategoryResponse{}

	for _, c := range categories {
		res = append(res, &models.CategoryResponse{
			ID:     c.ID,
			Name:   c.Name,
			ImgURL: c.ImgURL,
		})
	}

	return res
}
