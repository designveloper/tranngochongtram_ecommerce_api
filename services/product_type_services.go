package services

import (
	models "github.com/dsv/aware/models/response"
	"github.com/dsv/aware/repositories"
)

type ProductTypeServices interface {
	GetAllProductTypes() []*models.ProductTypeResponse
}

var ptsv *productTypeService

type productTypeService struct{}

// GetProductTypeService return a instance of ProductTypeServices
func GetProductTypeService() ProductTypeServices {
	once.Do(func() {
		ptsv = &productTypeService{}
	})

	return ptsv
}

func (ptsv *productTypeService) GetAllProductTypes() []*models.ProductTypeResponse {
	productTypes, err := repositories.GetProductTypeRepository().GetAllProductTypes()
	if err != nil {
		panic(err)
	}

	res := []*models.ProductTypeResponse{}

	for _, c := range productTypes {
		res = append(res, &models.ProductTypeResponse{
			ID:   c.ID,
			Name: c.Name,
		})
	}

	return res
}
