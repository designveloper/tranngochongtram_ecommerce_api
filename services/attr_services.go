package services

import (
	models "github.com/dsv/aware/models/response"
	"github.com/dsv/aware/repositories"
)

type AttributeServices interface {
	GetAllAttributes() []*models.AttributeResponse
}

var asv *attrService

type attrService struct{}

// GetAttributeService return a instance of AttributeServices
func GetAttributeService() AttributeServices {
	once.Do(func() {
		asv = &attrService{}
	})

	return asv
}

func (asv *attrService) GetAllAttributes() []*models.AttributeResponse {
	attributes, err := repositories.GetAttributeRepository().GetAllAttributes()
	if err != nil {
		panic(err)
	}

	res := []*models.AttributeResponse{}

	for _, a := range attributes {
		res = append(res, &models.AttributeResponse{
			ID:   a.ID,
			Name: a.Name,
		})
	}
	return res
}
