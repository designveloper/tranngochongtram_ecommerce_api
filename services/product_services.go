package services

import (
	"github.com/dsv/aware/models/response"
	"github.com/dsv/aware/repositories"
)

// ProductServices interface
type ProductServices interface {
	FilterProducts([]int, int, int) []*models.ProductResponse
}

var psv *productService

type productService struct{}

// GetProductService return a instance of ProductServices
func GetProductService() ProductServices {
	once.Do(func() {
		psv = &productService{}
	})

	return psv
}

func (psv *productService) FilterProducts(proAttrValueIDs []int, subCateID int, proTypeID int) []*models.ProductResponse {
	products, err := repositories.GetProductRepository().FilterProducts(proAttrValueIDs, subCateID, proTypeID)
	if err != nil {
		panic(err)
	}

	res := []*models.ProductResponse{}

	for _, p := range products {
		res = append(res, &models.ProductResponse{
			ID:            p.ID,
			Name:          p.Name,
			ProductTypeID: p.ProductTypeID,
			SubCategoryID: p.SubCategoryID,
			Price:         p.Price,
			ImgURL:        p.ImgURL,
		})
	}

	return res
}
