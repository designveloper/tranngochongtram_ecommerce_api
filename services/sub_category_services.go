package services

import (
	"github.com/dsv/aware/models/response"
	"github.com/dsv/aware/repositories"
)

// SubCategoryServices interface
type SubCategoryServices interface {
	GetAllSubCategories() []*models.SubCategoryResponse
	GetSubCategoriesByID(id int) []*models.SubCategoryResponse
}

var scsv *subCategoryService

type subCategoryService struct{}

// GetSubCategoryService return a instance of SubCategoryServices
func GetSubCategoryService() SubCategoryServices {
	once.Do(func() {
		scsv = &subCategoryService{}
	})

	return scsv
}

func (scsv *subCategoryService) GetAllSubCategories() []*models.SubCategoryResponse {
	subCategories, err := repositories.GetSubCategoryRepository().GetAllSubCategories()
	if err != nil {
		panic(err)
	}

	res := []*models.SubCategoryResponse{}

	for _, c := range subCategories {
		res = append(res, &models.SubCategoryResponse{
			ID:         c.ID,
			Name:       c.Name,
			CategoryID: c.CategoryID,
		})
	}

	return res
}

func (scsv *subCategoryService) GetSubCategoriesByID(id int) []*models.SubCategoryResponse {
	subCategories, err := repositories.GetSubCategoryRepository().GetSubCategoriesByID(id)
	if err != nil {
		panic(err)
	}

	res := []*models.SubCategoryResponse{}

	for _, c := range subCategories {
		res = append(res, &models.SubCategoryResponse{
			ID:         c.ID,
			Name:       c.Name,
			CategoryID: c.CategoryID,
		})
	}

	return res
}
