package services

import (
	dataModel "github.com/dsv/aware/models/data"
	reqModel "github.com/dsv/aware/models/request"
	"github.com/dsv/aware/models/response"

	"github.com/dsv/aware/repositories"
)

// UserService interface
type UserService interface {
	GetUserByID(id int) *models.UserResponse
	CreateUser(newUser *reqModel.UserRequest) int
}

var usv *userService

type userService struct{}

// GetUserService return a instance of UserService
func GetUserService() UserService {
	once.Do(func() {
		usv = &userService{}
	})

	return usv
}

func (usv *userService) GetUserByID(id int) *models.UserResponse {
	user, err := repositories.GetUserRepository().GetByID(id)
	if err != nil {
		panic(err)
	}
	return &models.UserResponse{
		ID:    user.ID,
		Name:  user.Name,
		Email: user.Email,
	}
}

func (usv *userService) CreateUser(newUser *reqModel.UserRequest) int {
	user := &dataModel.User{Email: newUser.Email, Name: newUser.Name}
	id, err := repositories.GetUserRepository().Create(user)
	if err != nil {
		panic(err)
	}
	return id
}
