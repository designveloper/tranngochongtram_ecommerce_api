package models

//ProductAttributeValue represent structure of product_attribute_value table
type ProductAttributeValue struct {
	ID               int `db:"id"`
	ProductID        int `db:"product_id"`
	AttributeValueID int `db:"attribute_value_id"`
	ProductTypeID    int `db:"product_type_id"`
	SubCategoryID    int `db:"sub_category_id"`
}
