package models

//Product represent structure of product table
type Product struct {
	ID            int    `db:"id"`
	Name          string `db:"name"`
	Deactive      bool   `db:"deactive"`
	ProductTypeID int    `db:"product_type_id"`
	SubCategoryID int    `db:"sub_category_id"`
	Price         uint64 `db:"price"`
	ImgURL        string `db:"img_url"`
}
