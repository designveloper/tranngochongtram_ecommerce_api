package models

// SubCategory represent structure of sub_category table
type SubCategory struct {
	ID         int    `db:"id"`
	Name       string `db:"name"`
	CategoryID int    `db:"category_id"`
}
