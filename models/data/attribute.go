package models

// Attribute represent structure of attribute table
type Attribute struct {
	ID       int    `db:"id"`
	Name     string `db:"name"`
	Deactive bool   `db:"deactive"`
}
