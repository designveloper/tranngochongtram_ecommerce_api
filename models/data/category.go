package models

//Category represent structure of category table
type Category struct {
	ID       int    `db:"id"`
	Name     string `db:"name"`
	ImgURL   string `db:"img_url"`
	Deactive bool   `db:"deactive"`
}
