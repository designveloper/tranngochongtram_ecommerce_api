package models

// ProductType represent structure of product_type table
type ProductType struct {
	ID   int    `db:"id"`
	Name string `db:"name"`
}
