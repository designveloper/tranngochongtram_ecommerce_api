package models

// AttributeValue represent structure of attribute_value table
type AttributeValue struct {
	ID               int    `db:"id"`
	AttributeValueID int    `db:"attribute_value_id"`
	Deactive         bool   `db:"deactive"`
	AttributeValue   string `db:"attribute_value"`
}
