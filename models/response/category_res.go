package models

// CategoryResponse present a model to response
type CategoryResponse struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	ImgURL string `json:"img_url"`
}
