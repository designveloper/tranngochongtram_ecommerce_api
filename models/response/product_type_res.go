package models

// ProductTypeResponse present a model to response
type ProductTypeResponse struct {
	ID   int    `json:"ID"`
	Name string `json:"name"`
}
