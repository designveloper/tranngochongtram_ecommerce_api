package models

// AttributeResponse present a model to response
type AttributeResponse struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
