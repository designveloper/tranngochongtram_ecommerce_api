package models

// SubCategoryResponse present a model to response
type SubCategoryResponse struct {
	ID         int    `json:"id"`
	Name       string `json:"name"`
	CategoryID int    `json:"category_id"`
}
