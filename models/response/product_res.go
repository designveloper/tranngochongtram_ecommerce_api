package models

// ProductResponse present a model to response
type ProductResponse struct {
	ID            int    `json:"id"`
	Name          string `json:"name"`
	ProductTypeID int    `json:"product_type_id"`
	SubCategoryID int    `json:"sub_category_id"`
	Price         uint64 `json:"price"`
	ImgURL        string `json:"img_url"`
}
