package models

type AttributeValueCombine struct {
	AttrValID int    `db:"attribute_value_id"`
	AttrVale  string `db:"attribute_value"`
	AttrID    int    `db:"attribute_id"`
	Name      string `db:"name"`
}
