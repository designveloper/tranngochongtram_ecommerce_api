package utils

import (
	"os"
	"sync"
)

// Config present configuration type
type Config struct {
	DbURL string
	HOST  string
	PORT  string
}

var (
	config *Config
	once   sync.Once
)

// GetConfig return a application config
func GetConfig() *Config {
	once.Do(getConfig)
	return config
}

func getConfig() {
	dbURL := os.Getenv("DATABASE_URL")
	if dbURL == "" {
		panic("Please specify database connection string via environment variable!")
	}

	host := os.Getenv("HOST")
	if host == "" {
		host = "127.0.0.1"
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}

	config = &Config{
		DbURL: dbURL,
		HOST:  host,
		PORT:  port,
	}
}
