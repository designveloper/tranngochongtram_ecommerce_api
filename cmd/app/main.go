package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/rs/cors"

	"github.com/dsv/aware/router"
	"github.com/dsv/aware/utils"
)

// setupGlobalMiddleware will setup CORS
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	handleCORS := cors.Default().Handler
	return handleCORS(handler)
}

func main() {
	config := utils.GetConfig()

	// repositories.GetProductRepository().FilterProducts([]int{1, 2, 3}, 3, 0)

	log.Fatal(http.ListenAndServe(
		fmt.Sprintf("%s:%s", config.HOST, config.PORT),
		setupGlobalMiddleware(router.NewRouter())),
	)
}
